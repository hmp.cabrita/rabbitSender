import { RabbitmqService } from '@lib/shared';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { RabbitTesterModule } from './rabbit-tester.module';

async function bootstrap() {
  const app = await NestFactory.create(RabbitTesterModule);
  const configService = app.get(ConfigService);
  const rabbitmqService = app.get<RabbitmqService>(RabbitmqService);
  app.connectMicroservice(rabbitmqService.getOptions('INVENTORY'));
  await app.startAllMicroservices();
  await app.listen(configService.get('PORT_RABBIT_TESTER'));
  console.log('Test.rabbit started');
}
bootstrap();
