import { IsNotEmpty, IsMongoId, IsOptional } from 'class-validator';
import { Types } from 'mongoose';

export class productPairDto {
  @IsMongoId()
  @IsNotEmpty()
  modelId: Types.ObjectId;

  @IsMongoId()
  @IsOptional()
  variantId: Types.ObjectId | '';
}
