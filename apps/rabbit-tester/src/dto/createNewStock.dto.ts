import { IsInt, IsMongoId, IsNotEmpty, IsOptional } from 'class-validator';
import { Types } from 'mongoose';

export class createNewStockDto {
  @IsMongoId()
  @IsNotEmpty()
  warehouseId: Types.ObjectId;

  @IsMongoId()
  @IsNotEmpty()
  projectId: Types.ObjectId;

  @IsMongoId()
  @IsNotEmpty()
  modelId: Types.ObjectId;

  @IsMongoId()
  variantId: Types.ObjectId | null;

  @IsInt()
  @IsNotEmpty()
  stock: number;

  @IsInt()
  @IsNotEmpty()
  reorderAmount: number;
}
