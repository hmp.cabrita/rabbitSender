import { IsMongoId, IsOptional, IsString } from 'class-validator';
import { Types as mongoDB } from 'mongoose';
// import { Type } from 'class-transformer';

// import { productPairDto } from './productPair.dto';

export class getStockDto {
  @IsMongoId()
  @IsOptional()
  projectId: mongoDB.ObjectId;

  @IsMongoId()
  @IsOptional()
  modelId: mongoDB.ObjectId;

  @IsMongoId()
  @IsOptional()
  variantId: mongoDB.ObjectId;

  @IsString()
  @IsOptional()
  // @IsArray()
  // @Type(() => productPairDto)
  // productPairs: productPairDto[];
  productPairs: string;
}
