import {
  IsInt,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class createEntryDto {
  @IsMongoId()
  @IsNotEmpty()
  warehouseId: string;

  @IsMongoId()
  @IsNotEmpty()
  projectId: string;

  @IsMongoId()
  @IsNotEmpty()
  modelId: string;

  @IsMongoId()
  @IsOptional()
  variantId?: string | null;

  @IsString()
  @IsNotEmpty()
  actionType: string;

  @IsInt()
  @IsNotEmpty()
  quantity: number;

  @IsInt()
  @IsNotEmpty()
  currentStock: number;
}
