import { IsInt, IsMongoId, IsNotEmpty, IsOptional } from 'class-validator';
import { Types } from 'mongoose';

export class createStockRawDto {
  @IsMongoId()
  @IsNotEmpty()
  warehouseId: Types.ObjectId;

  @IsMongoId()
  @IsNotEmpty()
  projectId: Types.ObjectId;

  @IsMongoId()
  @IsNotEmpty()
  modelId: Types.ObjectId;

  @IsMongoId()
  @IsOptional()
  variantId?: Types.ObjectId;

  @IsInt()
  @IsNotEmpty()
  stock: number;
}
