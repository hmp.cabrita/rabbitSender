import { Test, TestingModule } from '@nestjs/testing';
import { RabbitTesterController } from './rabbit-tester.controller';
import { RabbitTesterService } from './rabbit-tester.service';

describe('RabbitTesterController', () => {
  let rabbitTesterController: RabbitTesterController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [RabbitTesterController],
      providers: [RabbitTesterService],
    }).compile();

    rabbitTesterController = app.get<RabbitTesterController>(
      RabbitTesterController,
    );
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(rabbitTesterController.getHello()).toBe('Hello World!');
    });
  });
});
