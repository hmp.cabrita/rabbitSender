import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Types } from 'mongoose';
import { INVENTORY_QUEUE } from '@lib/shared';
import { createStockRawDto, getStockDto } from './dto';
import { lastValueFrom } from 'rxjs';
import { arch } from 'os';

@Injectable()
export class RabbitTesterService {
  constructor(@Inject(INVENTORY_QUEUE) private inventoryClient: ClientProxy) {}

  getHello(): string {
    return 'Hello World!';
  }

  async test(data: any) {
    console.log('rabbitmq service');
    const value = this.sendMessage('test', data);
    console.log(value);
    return value;
  }

  async createStock(data: createStockRawDto) {
    return this.sendMessage('create_stock', data);
  }

  async getStocks(warehouseId: Types.ObjectId, queryInput: getStockDto) {
    const data = {
      warehouseId,
      optionalData: {
        queryInput,
      },
    };
    return this.sendMessage('get_stock', data);
  }

  async sendMessage(action: string, data: any) {
    console.log(action);
    let value;
    let counter = 0;
    while (!value && counter < 10) {
      try {
        value = await lastValueFrom(this.inventoryClient.send(action, data));
      } catch {}
      counter++;
      console.log(counter);
    }
    if (!value) {
      console.log('no connection found');
    }
    return value;
  }
}
