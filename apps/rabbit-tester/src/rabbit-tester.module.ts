import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as joi from 'joi';
import { INVENTORY_QUEUE, RabbitmqModule } from '@lib/shared';
import { RabbitTesterController } from './rabbit-tester.controller';
import { RabbitTesterService } from './rabbit-tester.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: joi.object({
        RABBIT_MQ_URI: joi.string().required(),
        RABBIT_MQ_INVENTORY_QUEUE: joi.string().required(),
        PORT_RABBIT_TESTER: joi.number().required(),
      }),
      envFilePath: ['./.env'],
    }),
    RabbitmqModule.register({
      name: INVENTORY_QUEUE,
    }),
  ],
  controllers: [RabbitTesterController],
  providers: [RabbitTesterService],
})
export class RabbitTesterModule {}
