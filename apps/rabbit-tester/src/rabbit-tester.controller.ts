import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
} from '@nestjs/common';
import { EventPattern, Payload } from '@nestjs/microservices';
import { Types } from 'mongoose';
import { RabbitTesterService } from './rabbit-tester.service';
import { createStockRawDto, getStockDto } from './dto';
import { response } from 'express';

@Controller('tester')
export class RabbitTesterController {
  constructor(private readonly rabbitTesterService: RabbitTesterService) {}

  @Get()
  getHello(): string {
    return this.rabbitTesterService.getHello();
  }

  @Post('test')
  testRabbitmq(@Body() data: any) {
    console.log('rabbitmq controller', data);
    return this.rabbitTesterService.test(data);
  }

  @Post()
  async createStock(@Body() data: createStockRawDto) {
    return this.rabbitTesterService.createStock(data);
  }

  @Post(':id')
  @HttpCode(HttpStatus.OK)
  getStocks(
    @Param('id') warehouseId: Types.ObjectId,
    @Body() queryInput: getStockDto,
  ) {
    return this.rabbitTesterService.getStocks(warehouseId, queryInput);
  }

  // @EventPattern('response')
  // sendResponde(@Payload() reponse: any) {
  //   console.log(response);
  // }
}
